var express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestJson = require('request-json');
var urlMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=MeiVpeZEipyAl58H4hyl-Gmvdr0ghXk4";

var mongoClient = require('mongodb').MongoClient;
var urlMongo = "mongodb://localhost:27017/local";

var pg = require('pg');
var urlClients = "postgres://docker:docker@localhost:5433/bdseguridad";
// var urlClients = new pg.Client({user: 'docker', password: 'docker', host: 'localhost', port: 5433, database: 'bdseguridad'});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);


app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.send('Hola Mundo');
});

app.get('/movimientos', function(req, res) {
    var client = requestJson.createClient(urlMlab);
    client.get('', function(err, resMlab, body) {
        if (err) {
          console.log("Error: " + err + " body: " + body);
          res.send(err);
        }
        else {
          console.log("Success, body: " + body);
          res.send(body);
        }
    });
});

app.get('/movimientosMongo', function(req, res) {
    mongoClient.connect(urlMongo, function(err, db) {
      if (err) {
        console.log("Error: " + err);
        res.send(err);
      }
      else {
        console.log("Success, db: " + db);

        var col = db.collection('movimientos');

        col.find({}).limit(3).toArray(function(error, docs) {
          res.send(docs);
        });

        db.close();
      }
    });
});

app.post('/', function(req, res) {
    res.send('Hemos recibido su peticion');
});

app.post('/movimientos', function(req, res) {
    var client = requestJson.createClient(urlMlab);

    // var data = {
    //   idcliente: 1,
    //   title: 'Pedro',
    //   content: 'Post'
    // };

    // var data = '{"idcliente":1, "nombre":"Pedro", "apellido":"Post"}';

    console.log("req.body=" + req.body);

    client.post('', req.body, function(err, resMlab, body) {
        if (err) {
          console.log("Error: " + err + " body: " + body);
          res.send(err);
        }
        else {
          console.log("Success, body: " + body);
          res.send(body);
        }
    });
});

app.post('/login', function(req, res) {
  var clientPostgre = new pg.Client(urlClients);
  clientPostgre.connect();

  // asumo que recibo req.body = {usuario:xxxx, password: yyyy}

  const query = clientPostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password], 
    (err, result) => 
  {
    if (err) {
      console.log(err);
      res.send(err);
    }
    else {
      if (result.rows[0].count > 0) {
        console.log(result.rows[0]);
        res.send('Login correcto');
      }
      else {
        console.log(result.rows[0]);
        res.send('Login incorrecto');
      }
    }
  });
});


app.use('/a', express.static(path.join(__dirname, 'app')));

app.get('/clientes', function(req, res) {
    //res.send('Aqui tiene el listado de clientes');
    res.send(JSON.stringify({"msg" : "Aqui tiene un listado de clientes", "clients" : []}));
    //res.SendFile(path.join(__dirname, 'fichero.txt');
});

app.get('/clientes/:idcliente', function(req, res) {
    //res.send('Aqui tiene el listado de clientes');
    res.send(JSON.stringify({"msg" : "Aqui tiene al cliente "+req.params.idcliente, "clients" : [req.params.idcliente]}));
    //res.SendFile(path.join(__dirname, 'fichero.txt');
});

app.post('/clientes', function(req, res) {
    res.send(JSON.stringify({"msg" : "Ha creado un cliente satisfactoriamente"}));
});

app.put('/clientes/:idcliente', function(req, res) {
    res.send(JSON.stringify({"msg" : "Ha modificado al cliente "+req.params.idcliente+" satisfactoriamente"}));
});

app.delete('/clientes/:idcliente', function(req, res) {
    res.send(JSON.stringify({"msg" : "Ha borrado al cliente "+ req.params.idcliente}));
});
