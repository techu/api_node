# Imagen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copio archivos
ADD . /app

# Dependencias
RUN npm install

# Puerto
EXPOSE 3000

# Command
CMD ["npm", "start"]
